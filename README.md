# trumpinsane [coin] #

----

- Algorithm: X11
- Acronym: INSANE [sic]
- INSANE count: 21,000,000
- Reward system: Fully PoS
- Interest: 2 per cent

----

![splash.png](https://cdn.pbrd.co/images/9TwUAp0Gb.png)

----


**See the [trumpinsane wiki](http://trumpinsane.pw/wiki.html) for further details.**

----

**Ports
**

- rpcport=4867
- port=4866


----

**Wallet (choose either link 1 or link 2)
**

- [Windows-qt*](https://storage-us.mail.com/qx/mail_com/?locale=en&guestToken=WlAZnsy0SHaRZQj-3QR6Vw&loginName=trumpinsane@mail.com)
- [Windows-qt*](https://mega.nz/#!Jx5UlYaY!Rr0UVATQoA9ezRXzsAXmVvh2kl2SZoAEFi5Q_B7U3tE)

----


### Daemon and deps ###

**Using QT4**

sudo apt-get install git build-essential libssl-dev libboost-all-dev libdb++-dev libminiupnpc-dev qt-sdk -y


### Compile Ubuntu and Debian ###

make -f makefile.unix

----

***Wallet***

qmake -qt=qt4

make

----

### Blockexplorer ###

[TrumpInsane Explorer](http://104.236.121.235:3001/)

----

### All INSANE is to be given away ###

More details at [All Free](http://trumpinsane.pw/wiki.html#qualifying-for-the-giveaway)
